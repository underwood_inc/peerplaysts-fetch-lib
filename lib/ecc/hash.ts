import createHash from 'create-hash';
import createHmac from 'create-hmac';

/**
 * Create sha1 hash.
 * @param data - Buffer or String to create hash from.
 * @param encoding - 'hex' | 'binary' | 'base64'
 */
function sha1(data: string|Buffer, encoding: createHash.encoding) {
  return createHash('sha1')
    .update(data)
    .digest(encoding);
}

/**
 * Create sha256 hash.
 * @param data - Buffer or String to create hash from.
 * @param encoding - 'hex' | 'binary' | 'base64'
 */
function sha256(data: string|Buffer, encoding?: createHash.encoding) {
  return createHash('sha256')
    .update(data)
    .digest(encoding);
}

/**
 * Create sha512 hash.
 * @param data - Buffer or String to create hash from.
 * @param encoding - 'hex' | 'binary' | 'base64'
 */
function sha512(data: Buffer, encoding?: createHash.encoding) {
  return createHash('sha512')
    .update(data)
    .digest(encoding);
}

/**
 * Create a HmacShar256 Buffer.
 * @param buffer - Buffer or String to create hash from.
 * @param secret - key to use during 
 */
function HmacSHA256(buffer: Buffer, secret: string) {
  return createHmac('sha256', secret)
    .update(buffer)
    .digest();
}

/**
 * Create rmd160 Buffer.
 * @param data - Buffer or String to create buffer from.
 */
function ripemd160(data: string|Buffer) {
  return createHash('rmd160')
    .update(data)
    .digest();
}

export {
  sha1, sha256, sha512, HmacSHA256, ripemd160
};